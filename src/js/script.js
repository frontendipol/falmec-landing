$('.carousel-wrapper').slick({
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    prevArrow: '<div class="carousel-arrow a-prev"><img src="img/prev.svg"></div>',
    nextArrow: '<div class="carousel-arrow a-next"><img src="img/next.svg"></div>',
    fade: false,
    dots: false,
    //arrows: false,
    infinite: true,
  // asNavFor: '.product-page .slider-nav',
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
        }
      },
      {
        breakpoint: 960,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
});

$('.carousel-wrapper__item').slick({
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    prevArrow: '<div class="carousel-arrow a-prev"><img src="img/prev.svg"></div>',
    nextArrow: '<div class="carousel-arrow a-next"><img src="img/next.svg"></div>',
    fade: false,
    dots: false,
    arrows: true,
    infinite: true,
  // asNavFor: '.product-page .slider-nav',
     variableWidth: true,
     responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        }
      },
      {
        breakpoint: 960,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
});

//бургер меню
$(function() {

  $(".auth-lk").on('click', function() {
    $('.auth-menu').toggleClass('auth-menu__active');
    $('.auth-menu').fadeIn(300);
  });

  $(".auth-menu-close").on('click', function() { 
    $(".auth-menu").toggleClass('auth-menu__active');
    $('.auth-menu').fadeOut(350);
  });
  
});

 //бургер меню
$(function() {

  $(".btn-modal").on('click', function() {
    $('.feedback-modal').toggleClass('show');
    $('.feedback-modal').fadeIn(300);
  });

  $(".feedback-modal-close").on('click', function() { 
    $(".feedback-modal").toggleClass('show');
    $('.feedback-modal').fadeOut(350);
  });
  
});
